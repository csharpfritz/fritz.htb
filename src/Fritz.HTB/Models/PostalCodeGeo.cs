﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fritz.HTB.Models
{
    public class PostalCodeGeo
    {

		public string PostalCode { get; set; }

		public decimal Latitude { get; set; }

		public decimal Longitude { get; set; }

	}
}
