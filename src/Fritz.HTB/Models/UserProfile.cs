﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fritz.HTB.Models
{

	/// <summary>
	/// Information about a user that they want to share with the organizations in HTB
	/// </summary>
	public class UserProfile
    {

		[Key]
		public string UserId { get; set; }

		public string GivenName { get; set; }

		public string Surname { get; set; }

		public DateTime? BirthDate { get; set; }

		public string EmailAddress { get; set; }

		/// <summary>
		/// Tenants to which this user participates
		/// </summary>
		public List<TenantUsers> TenantUsers { get; set; }

	}

}
