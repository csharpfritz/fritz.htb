﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;

namespace Fritz.HTB.Models
{

	public class HtbContext : DbContext
    {

		public HtbContext(DbContextOptions<HtbContext> options) : base(options)
		{

		}


		public DbSet<Tenant> Tenants { get; set; }

		public DbSet<UserProfile> UserProfiles { get; set; }

		public DbSet<UserSecurity> Security { get; set; }

		public DbSet<Campaign> Campaigns { get; set; }

		public DbSet<Activity> Activities { get; set; }

		public DbSet<Location> Locations { get; set; }

		public DbSet<PostalCodeGeo> PostalCodes { get; set; }

		public DbSet<HtbTask> Tasks { get; set; }


		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{

			modelBuilder.ForSqlServer().UseIdentity();

			modelBuilder.Entity<PostalCodeGeo>()
				.Key(k => k.PostalCode);

			modelBuilder.Entity<UserProfile>()
				.Key(k => k.UserId);

			modelBuilder.Entity<UserSecurity>()
				.Key(k => k.UserId);

			base.OnModelCreating(modelBuilder);
		}

	}

}
