﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fritz.HTB.Models
{
	public class TaskUsers
	{

		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		public HtbTask Task { get; set; }

		public UserProfile User { get; set; }

		public DateTime StatusDateTimeUtc { get; set; } = DateTime.UtcNow;

		public string Status { get; set; }

		public string StatusDescription { get; set; }

	}
}