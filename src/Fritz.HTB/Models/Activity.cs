﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fritz.HTB.Models
{
	public class Activity
	{

		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		public string Name { get; set; }

		public DateTime StartDateTimeUtc { get; set; }

		public DateTime EndDateTimeUtc { get; set; }

		public Location Location { get; set; }

		public List<HtbTask> Tasks { get; set; }

	}
}