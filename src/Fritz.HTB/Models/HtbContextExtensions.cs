﻿using Microsoft.AspNet.Builder;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Relational.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fritz.HTB.Models
{
    public static class HtbContextExtensions
    {

		public static void EnsureSampleData(this IApplicationBuilder app)
		{

			var ctx = (HtbContext)app.ApplicationServices.GetService(typeof(HtbContext));
			if (ctx.AllMigrationsApplied())
			{

				if (!ctx.Tenants.Any())
				{
					ctx.Tenants.Add(new Tenant { Name = "Red Cross" });
					ctx.Tenants.Add(new Tenant { Name = "United Way" });
					ctx.Tenants.Add(new Tenant { Name = "Salvation Army" });
					ctx.Tenants.Add(new Tenant { Name = "Variety Club" });
					ctx.Tenants.Add(new Tenant { Name = "YMCA" });

					ctx.SaveChanges();

				}

			}

		}

    }

	public static class DbContextExtensions
	{
		public static bool AllMigrationsApplied(this DbContext context)
		{
			return !((IAccessor<Migrator>)context.Database.AsRelational()).Service.GetUnappliedMigrations().Any();
		}
	}
}
