﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Fritz.HTB.Models
{

	public class TenantUsers
    {

		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		[ForeignKey("UserProfile")]
		public int UserId { get; set; }

		[ForeignKey("Tenant")]
		public int TenantId { get; set; }

		public TenantSecurity AccessLevel { get; set; }

	}

}
