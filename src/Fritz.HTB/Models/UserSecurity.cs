﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fritz.HTB.Models
{

	/// <summary>
	/// Security information about out users in the application.  Contains appropriate information to link back to OAuth providers
	/// </summary>
	public class UserSecurity
    {

		public string UserId { get; set; }

	}

}
