﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fritz.HTB.Models
{
	public class Campaign
	{

		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		public Tenant ManagingTenant { get; set; }

		/// <summary>
		/// Collection of Tenants that are supporting this campaign
		/// </summary>
		public List<Tenant> ParticipatingTenants { get; set; }

		/// <summary>
		/// The date the campaign starts
		/// </summary>
		public DateTime StartDateTimeUtc { get; set; }

		/// <summary>
		/// The date the campaign ends
		/// </summary>
		public DateTime EndDateTimeUtc { get; set; }

		public List<Activity> Activities { get; set; }

	}
}