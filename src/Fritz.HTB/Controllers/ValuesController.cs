﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fritz.HTB.Models;
using Microsoft.AspNet.Mvc;

namespace Fritz.HTB.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
		private HtbContext _DbContext;

		public ValuesController(Models.HtbContext ctx)
		{
			_DbContext = ctx;
		}

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {

			return _DbContext.Tenants.Select(t => t.Name);

            
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
