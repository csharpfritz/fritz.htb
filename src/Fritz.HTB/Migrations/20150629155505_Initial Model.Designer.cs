using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Metadata.Builders;
using Microsoft.Data.Entity.Relational.Migrations.Infrastructure;
using Fritz.HTB.Models;

namespace Fritz.HTB.Migrations
{
    [ContextType(typeof(HtbContext))]
    partial class InitialModel
    {
        public override string Id
        {
            get { return "20150629155505_Initial Model"; }
        }
        
        public override string ProductVersion
        {
            get { return "7.0.0-beta4-12943"; }
        }
        
        public override IModel Target
        {
            get
            {
                var builder = new BasicModelBuilder()
                    .Annotation("SqlServer:ValueGeneration", "Identity");
                
                builder.Entity("Fritz.HTB.Models.Activity", b =>
                    {
                        b.Property<int?>("CampaignId")
                            .Annotation("OriginalValueIndex", 0)
                            .Annotation("ShadowIndex", 0);
                        b.Property<DateTime>("EndDateTimeUtc")
                            .Annotation("OriginalValueIndex", 1);
                        b.Property<int>("Id")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 2)
                            .Annotation("SqlServer:ValueGeneration", "Default");
                        b.Property<int?>("LocationId")
                            .Annotation("OriginalValueIndex", 3)
                            .Annotation("ShadowIndex", 1);
                        b.Property<string>("Name")
                            .Annotation("OriginalValueIndex", 4);
                        b.Property<DateTime>("StartDateTimeUtc")
                            .Annotation("OriginalValueIndex", 5);
                        b.Key("Id");
                    });
                
                builder.Entity("Fritz.HTB.Models.Campaign", b =>
                    {
                        b.Property<DateTime>("EndDateTimeUtc")
                            .Annotation("OriginalValueIndex", 0);
                        b.Property<int>("Id")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 1)
                            .Annotation("SqlServer:ValueGeneration", "Default");
                        b.Property<DateTime>("StartDateTimeUtc")
                            .Annotation("OriginalValueIndex", 2);
                        b.Key("Id");
                    });
                
                builder.Entity("Fritz.HTB.Models.HtbTask", b =>
                    {
                        b.Property<int?>("ActivityId")
                            .Annotation("OriginalValueIndex", 0)
                            .Annotation("ShadowIndex", 0);
                        b.Property<string>("Description")
                            .Annotation("OriginalValueIndex", 1);
                        b.Property<DateTime?>("EndDateTimeUtc")
                            .Annotation("OriginalValueIndex", 2);
                        b.Property<int>("Id")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 3)
                            .Annotation("SqlServer:ValueGeneration", "Default");
                        b.Property<string>("Name")
                            .Annotation("OriginalValueIndex", 4);
                        b.Property<DateTime?>("StartDateTimeUtc")
                            .Annotation("OriginalValueIndex", 5);
                        b.Key("Id");
                    });
                
                builder.Entity("Fritz.HTB.Models.Location", b =>
                    {
                        b.Property<string>("Address1")
                            .Annotation("OriginalValueIndex", 0);
                        b.Property<string>("Address2")
                            .Annotation("OriginalValueIndex", 1);
                        b.Property<string>("City")
                            .Annotation("OriginalValueIndex", 2);
                        b.Property<string>("Country")
                            .Annotation("OriginalValueIndex", 3);
                        b.Property<int>("Id")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 4)
                            .Annotation("SqlServer:ValueGeneration", "Default");
                        b.Property<string>("PostalCodePostalCode")
                            .Annotation("OriginalValueIndex", 5)
                            .Annotation("ShadowIndex", 0);
                        b.Property<string>("State")
                            .Annotation("OriginalValueIndex", 6);
                        b.Key("Id");
                    });
                
                builder.Entity("Fritz.HTB.Models.PostalCodeGeo", b =>
                    {
                        b.Property<decimal>("Latitude")
                            .Annotation("OriginalValueIndex", 0);
                        b.Property<decimal>("Longitude")
                            .Annotation("OriginalValueIndex", 1);
                        b.Property<string>("PostalCode")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 2);
                        b.Key("PostalCode");
                    });
                
                builder.Entity("Fritz.HTB.Models.TaskUsers", b =>
                    {
                        b.Property<int>("Id")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 0)
                            .Annotation("SqlServer:ValueGeneration", "Default");
                        b.Property<string>("Status")
                            .Annotation("OriginalValueIndex", 1);
                        b.Property<DateTime>("StatusDateTimeUtc")
                            .Annotation("OriginalValueIndex", 2);
                        b.Property<string>("StatusDescription")
                            .Annotation("OriginalValueIndex", 3);
                        b.Property<int?>("TaskId")
                            .Annotation("OriginalValueIndex", 4)
                            .Annotation("ShadowIndex", 0);
                        b.Property<string>("UserUserId")
                            .Annotation("OriginalValueIndex", 5)
                            .Annotation("ShadowIndex", 1);
                        b.Key("Id");
                    });
                
                builder.Entity("Fritz.HTB.Models.Tenant", b =>
                    {
                        b.Property<int>("Id")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 0)
                            .Annotation("SqlServer:ValueGeneration", "Default");
                        b.Property<string>("LogoUrl")
                            .Annotation("OriginalValueIndex", 1);
                        b.Property<string>("Name")
                            .Annotation("OriginalValueIndex", 2);
                        b.Property<string>("WebUrl")
                            .Annotation("OriginalValueIndex", 3);
                        b.Key("Id");
                    });
                
                builder.Entity("Fritz.HTB.Models.TenantUsers", b =>
                    {
                        b.Property<int>("AccessLevel")
                            .Annotation("OriginalValueIndex", 0);
                        b.Property<int>("Id")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 1)
                            .Annotation("SqlServer:ValueGeneration", "Default");
                        b.Property<int>("TenantId")
                            .Annotation("OriginalValueIndex", 2);
                        b.Property<int>("UserId")
                            .Annotation("OriginalValueIndex", 3);
                        b.Property<string>("UserProfileUserId")
                            .Annotation("OriginalValueIndex", 4)
                            .Annotation("ShadowIndex", 0);
                        b.Key("Id");
                    });
                
                builder.Entity("Fritz.HTB.Models.UserProfile", b =>
                    {
                        b.Property<DateTime?>("BirthDate")
                            .Annotation("OriginalValueIndex", 0);
                        b.Property<string>("EmailAddress")
                            .Annotation("OriginalValueIndex", 1);
                        b.Property<string>("GivenName")
                            .Annotation("OriginalValueIndex", 2);
                        b.Property<string>("Surname")
                            .Annotation("OriginalValueIndex", 3);
                        b.Property<string>("UserId")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 4);
                        b.Key("UserId");
                    });
                
                builder.Entity("Fritz.HTB.Models.UserSecurity", b =>
                    {
                        b.Property<string>("UserId")
                            .GenerateValueOnAdd()
                            .Annotation("OriginalValueIndex", 0);
                        b.Key("UserId");
                    });
                
                builder.Entity("Fritz.HTB.Models.Activity", b =>
                    {
                        b.ForeignKey("Fritz.HTB.Models.Campaign", "CampaignId");
                        b.ForeignKey("Fritz.HTB.Models.Location", "LocationId");
                    });
                
                builder.Entity("Fritz.HTB.Models.HtbTask", b =>
                    {
                        b.ForeignKey("Fritz.HTB.Models.Activity", "ActivityId");
                    });
                
                builder.Entity("Fritz.HTB.Models.Location", b =>
                    {
                        b.ForeignKey("Fritz.HTB.Models.PostalCodeGeo", "PostalCodePostalCode");
                    });
                
                builder.Entity("Fritz.HTB.Models.TaskUsers", b =>
                    {
                        b.ForeignKey("Fritz.HTB.Models.HtbTask", "TaskId");
                        b.ForeignKey("Fritz.HTB.Models.UserProfile", "UserUserId");
                    });
                
                builder.Entity("Fritz.HTB.Models.TenantUsers", b =>
                    {
                        b.ForeignKey("Fritz.HTB.Models.UserProfile", "UserProfileUserId");
                    });
                
                return builder.Model;
            }
        }
    }
}
