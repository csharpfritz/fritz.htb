using System.Collections.Generic;
using Microsoft.Data.Entity.Relational.Migrations;
using Microsoft.Data.Entity.Relational.Migrations.Builders;
using Microsoft.Data.Entity.Relational.Migrations.Operations;

namespace Fritz.HTB.Migrations
{
    public partial class InitialModel : Migration
    {
        public override void Up(MigrationBuilder migration)
        {
            migration.CreateTable(
                name: "Campaign",
                columns: table => new
                {
                    EndDateTimeUtc = table.Column(type: "datetime2", nullable: false),
                    Id = table.Column(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGeneration", "Identity"),
                    StartDateTimeUtc = table.Column(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaign", x => x.Id);
                });
            migration.CreateTable(
                name: "PostalCodeGeo",
                columns: table => new
                {
                    Latitude = table.Column(type: "decimal(18, 2)", nullable: false),
                    Longitude = table.Column(type: "decimal(18, 2)", nullable: false),
                    PostalCode = table.Column(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostalCodeGeo", x => x.PostalCode);
                });
            migration.CreateTable(
                name: "Tenant",
                columns: table => new
                {
                    Id = table.Column(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGeneration", "Identity"),
                    LogoUrl = table.Column(type: "nvarchar(max)", nullable: true),
                    Name = table.Column(type: "nvarchar(max)", nullable: true),
                    WebUrl = table.Column(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tenant", x => x.Id);
                });
            migration.CreateTable(
                name: "UserProfile",
                columns: table => new
                {
                    BirthDate = table.Column(type: "datetime2", nullable: true),
                    EmailAddress = table.Column(type: "nvarchar(max)", nullable: true),
                    GivenName = table.Column(type: "nvarchar(max)", nullable: true),
                    Surname = table.Column(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfile", x => x.UserId);
                });
            migration.CreateTable(
                name: "UserSecurity",
                columns: table => new
                {
                    UserId = table.Column(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSecurity", x => x.UserId);
                });
            migration.CreateTable(
                name: "Location",
                columns: table => new
                {
                    Address1 = table.Column(type: "nvarchar(max)", nullable: true),
                    Address2 = table.Column(type: "nvarchar(max)", nullable: true),
                    City = table.Column(type: "nvarchar(max)", nullable: true),
                    Country = table.Column(type: "nvarchar(max)", nullable: true),
                    Id = table.Column(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGeneration", "Identity"),
                    PostalCodePostalCode = table.Column(type: "nvarchar(450)", nullable: true),
                    State = table.Column(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Location_PostalCodeGeo_PostalCodePostalCode",
                        columns: x => x.PostalCodePostalCode,
                        referencedTable: "PostalCodeGeo",
                        referencedColumn: "PostalCode");
                });
            migration.CreateTable(
                name: "TenantUsers",
                columns: table => new
                {
                    AccessLevel = table.Column(type: "int", nullable: false),
                    Id = table.Column(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGeneration", "Identity"),
                    TenantId = table.Column(type: "int", nullable: false),
                    UserId = table.Column(type: "int", nullable: false),
                    UserProfileUserId = table.Column(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantUsers_UserProfile_UserProfileUserId",
                        columns: x => x.UserProfileUserId,
                        referencedTable: "UserProfile",
                        referencedColumn: "UserId");
                });
            migration.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    CampaignId = table.Column(type: "int", nullable: true),
                    EndDateTimeUtc = table.Column(type: "datetime2", nullable: false),
                    Id = table.Column(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGeneration", "Identity"),
                    LocationId = table.Column(type: "int", nullable: true),
                    Name = table.Column(type: "nvarchar(max)", nullable: true),
                    StartDateTimeUtc = table.Column(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Activity_Campaign_CampaignId",
                        columns: x => x.CampaignId,
                        referencedTable: "Campaign",
                        referencedColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Activity_Location_LocationId",
                        columns: x => x.LocationId,
                        referencedTable: "Location",
                        referencedColumn: "Id");
                });
            migration.CreateTable(
                name: "HtbTask",
                columns: table => new
                {
                    ActivityId = table.Column(type: "int", nullable: true),
                    Description = table.Column(type: "nvarchar(max)", nullable: true),
                    EndDateTimeUtc = table.Column(type: "datetime2", nullable: true),
                    Id = table.Column(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGeneration", "Identity"),
                    Name = table.Column(type: "nvarchar(max)", nullable: true),
                    StartDateTimeUtc = table.Column(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HtbTask", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HtbTask_Activity_ActivityId",
                        columns: x => x.ActivityId,
                        referencedTable: "Activity",
                        referencedColumn: "Id");
                });
            migration.CreateTable(
                name: "TaskUsers",
                columns: table => new
                {
                    Id = table.Column(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGeneration", "Identity"),
                    Status = table.Column(type: "nvarchar(max)", nullable: true),
                    StatusDateTimeUtc = table.Column(type: "datetime2", nullable: false),
                    StatusDescription = table.Column(type: "nvarchar(max)", nullable: true),
                    TaskId = table.Column(type: "int", nullable: true),
                    UserUserId = table.Column(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskUsers_HtbTask_TaskId",
                        columns: x => x.TaskId,
                        referencedTable: "HtbTask",
                        referencedColumn: "Id");
                    table.ForeignKey(
                        name: "FK_TaskUsers_UserProfile_UserUserId",
                        columns: x => x.UserUserId,
                        referencedTable: "UserProfile",
                        referencedColumn: "UserId");
                });
        }
        
        public override void Down(MigrationBuilder migration)
        {
            migration.DropTable("Activity");
            migration.DropTable("Campaign");
            migration.DropTable("HtbTask");
            migration.DropTable("Location");
            migration.DropTable("PostalCodeGeo");
            migration.DropTable("TaskUsers");
            migration.DropTable("Tenant");
            migration.DropTable("TenantUsers");
            migration.DropTable("UserProfile");
            migration.DropTable("UserSecurity");
        }
    }
}
